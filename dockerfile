# Dockerfile
FROM dreamacro/clash-premium
RUN set -e \
    && sed -i 's/dl-cdn.alpinelinux.org/mirrors.tuna.tsinghua.edu.cn/g' /etc/apk/repositories \
    && apk add --no-cache iptables inotify-tools tini \
    && wget https://mirror.ghproxy.com/https://github.com/haishanh/yacd/releases/download/v0.3.8/yacd.tar.xz \
    && tar xf yacd.tar.xz \
    && mv public ui
WORKDIR /app
COPY scripts/* /
RUN chmod +x /entrypoint.sh && chmod +x /iptables-down.sh && chmod +x /iptables.sh
#ENTRYPOINT ["tini", "--", "/entrypoint.sh"]
ENTRYPOINT ["tini","-g", "--", "/entrypoint.sh"]
